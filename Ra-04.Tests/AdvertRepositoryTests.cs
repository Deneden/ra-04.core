﻿using System;
using System.Collections.Generic;
using Ra_04.Domain.Models;
using Xunit;

namespace Ra_04.Tests
{
    public class AdvertRepositoryTests
    {
        [Fact]
        public void GetAdvertList()
        {
            var A = new[]
            {
                new Advert {
                    Id =1,
                    UserId = "1",
                    User = new ApplicationUser(),
                    Type = 1,
                    Cost = 100,
                    CreateTime = DateTime.Now,
                    ActualTime = DateTime.Now.AddDays(1),
                    EdiTime = DateTime.Now,
                    Views = 1,
                    Description = "Тестовое объявление",
                    Text = "Текст тестового объявления",
                    IsBlock  = false,
                    BlockReason = null,
                    IsDone = false,
                    UserUseId  = 0,
                    IsDelete = false,
                    AdvertHashtags = new List<AdvertHashtag>(),
                    Options = new List<Option>(),
                    Images = new List<Image>()
                },
                new Advert
                {
                    Id =2,
                    UserId = "1",
                    User = new ApplicationUser(),
                    Type = 1,
                    Cost = 100,
                    CreateTime = DateTime.Now,
                    ActualTime = DateTime.Now.AddDays(1),
                    EdiTime = DateTime.Now,
                    Views = 1,
                    Description = "Тестовое объявление2",
                    Text = "Текст тестового объявления2",
                    IsBlock  = false,
                    BlockReason = null,
                    IsDone = false,
                    UserUseId  = 0,
                    IsDelete = false,
                    AdvertHashtags = new List<AdvertHashtag>(),
                    Options = new List<Option>(),
                    Images = new List<Image>()
                }
            };
        }
    }
}
