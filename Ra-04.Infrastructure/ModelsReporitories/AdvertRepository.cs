﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Ra_04.Domain.Interfaces;
using Ra_04.Domain.Models;
using Ra_04.Infrastructure.Context;

namespace Ra_04.Infrastructure.ModelsReporitories
{
    public class AdvertRepository : IAdvert
    {
        private readonly ApplicationContext _db;
        private bool _disposed;

        public AdvertRepository(ApplicationContext db)
        {
            _db = db;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            _disposed = true;
        }

        public IEnumerable<Advert> GetAdvertList(string id)
        {
            if (id == null)
                return _db.Adverts.Include(x => x.User.ApplicationProfile).Include(y => y.AdvertHashtags).ThenInclude(z => z.Hashtag);
            return _db.Adverts.Include(x => x.User.ApplicationProfile).Include(y=>y.AdvertHashtags).ThenInclude(z=>z.Hashtag).Where(z => z.User.Id == id);
        }

        public IEnumerable<Advert> GetAdvertByTag(int hashtag)
        {
            return
                _db.Hashtags.Where(x => x.Id == hashtag)
                    .SelectMany(x => x.AdvertHashtags)
                    .Select(y => y.Advert)
                    .Include(z => z.User.ApplicationProfile)
                    .Include(a => a.AdvertHashtags).
                    ThenInclude(b => b.Hashtag);
        }

        public async Task<Advert> GetAdvert(int id)
        {
            return await _db.Adverts.Include(x => x.User).Include(a => a.AdvertHashtags).ThenInclude(b=>b.Hashtag).SingleOrDefaultAsync(i => i.Id == id);
        }

        public void Create(Advert item)
        {
            _db.Adverts.Add(item);
            _db.SaveChanges();
        }

        public void Update(Advert item)
        {
            _db.Entry(item).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            Advert advert = _db.Adverts.SingleOrDefault(i => i.Id == id);
            if (advert != null)
            {
                _db.Adverts.Remove(advert);
                _db.SaveChanges();
            }
        }
    }
}
