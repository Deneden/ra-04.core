﻿using System;
using System.Linq;
using Ra_04.Domain.Interfaces;
using Ra_04.Domain.Models;
using Ra_04.Infrastructure.Context;

namespace Ra_04.Infrastructure.ModelsReporitories
{
    public class HashtagRepository : IHashtag
    {
        private readonly ApplicationContext _db;
        private bool _disposed;

        public HashtagRepository(ApplicationContext db)
        {
            _db = db;
        }
        public Hashtag FindByName(string name)
        {
            return _db.Hashtags.SingleOrDefault(m => m.Name == name);
        }

        public Hashtag Add(string name)
        {
            Hashtag hashtag = new Hashtag
            {
                Name = name
            };

            _db.Hashtags.Add(hashtag);
            _db.SaveChanges();
            return hashtag;
        }

        public void Delete(int id)
        {
            Hashtag hashtag = _db.Hashtags.SingleOrDefault(i => i.Id == id);
            if (hashtag != null)
            {
                _db.Hashtags.Remove(hashtag);
                _db.SaveChanges();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
