﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Ra_04.Domain.Interfaces;
using Ra_04.Domain.Models;
using Ra_04.Infrastructure.Context;

namespace Ra_04.Infrastructure.ModelsReporitories
{
    public class RegionRepository : IRegion
    {
        private readonly ApplicationContext _db;
        private bool _disposed;

        public RegionRepository(ApplicationContext db)
        {
            _db = db;
        }

        public IEnumerable<Region> GetRegionList()
        {
            return _db.Regions.ToList();
        }

        public Task<Region> GetRegion(int id)
        {
            return _db.Regions.SingleOrDefaultAsync(i => i.Id == id);
        }

        public void Create(Region item)
        {
            _db.Regions.Add(item);
            _db.SaveChanges();
        }

        public void Update(Region item)
        {
            _db.Entry(item).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            Region region = _db.Regions.SingleOrDefault(i => i.Id == id);
            if (region != null)
            {
                _db.Regions.Remove(region);
                _db.SaveChanges();
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
