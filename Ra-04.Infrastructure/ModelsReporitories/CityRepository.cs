﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Ra_04.Domain.Interfaces;
using Ra_04.Domain.Models;
using Ra_04.Infrastructure.Context;

namespace Ra_04.Infrastructure.ModelsReporitories
{
    public class CityRepository : ICity
    {
        private readonly ApplicationContext _db;
        private bool _disposed;

        public CityRepository(ApplicationContext db)
        {
            _db = db;
        }

        public ICollection<City> GetCitiesList()
        {
            return _db.Cities.ToList();
        }

        public Task<City> GetCity(int id)
        {
            return _db.Cities.SingleOrDefaultAsync(i => i.Id == id);
        }

        public ICollection<City> GetCitiesByDistrict(int id)
        {
            return _db.Cities.Where(m => m.DistrictId == id).ToList();
        }

        public void Create(City item)
        {
            _db.Cities.Add(item);
            _db.SaveChanges();
        }

        public void Update(City item)
        {
            _db.Entry(item).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            City city = _db.Cities.SingleOrDefault(i => i.Id == id);
            if (city != null)
            {
                _db.Cities.Remove(city);
                _db.SaveChanges();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
