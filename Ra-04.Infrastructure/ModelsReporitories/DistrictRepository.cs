﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Ra_04.Domain.Interfaces;
using Ra_04.Domain.Models;
using Ra_04.Infrastructure.Context;

namespace Ra_04.Infrastructure.ModelsReporitories
{
 public class DistrictRepository : IDistrict
{
    private readonly ApplicationContext _db;
    private bool _disposed;

    public DistrictRepository(ApplicationContext db)
    {
        _db = db;
    }

    public ICollection<District> GetDistrictList()
    {
        return _db.Districts.ToList();
    }

    public Task<District> GetDistrict(int id)
    {
        return _db.Districts.SingleOrDefaultAsync(i => i.Id == id);
    }

    public ICollection<District> GetDistrictByRegions(int id)
    {
        return _db.Districts.Where(m => m.RegionId == id).ToList();
    }

    public void Create(District item)
    {
        _db.Districts.Add(item);
        _db.SaveChanges();
    }

    public void Update(District item)
    {
        _db.Entry(item).State = EntityState.Modified;
        _db.SaveChanges();
    }

    public void Delete(int id)
    {
        District district = _db.Districts.SingleOrDefault(i => i.Id == id);
        if (district != null)
        {
            _db.Districts.Remove(district);
            _db.SaveChanges();
        }
    }
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    public virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                _db.Dispose();
            }
        }
        _disposed = true;
    }
}
}
