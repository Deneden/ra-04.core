﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Ra_04.Domain.Models;

namespace Ra_04.Infrastructure.Context
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
    : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<AdvertHashtag>()
                .HasKey(t => new { t.AdvertId, t.HashtagId });

            modelBuilder.Entity<AdvertHashtag>()
                .HasOne(pt => pt.Advert)
                .WithMany(p => p.AdvertHashtags)
                .HasForeignKey(pt => pt.AdvertId);

            modelBuilder.Entity<AdvertHashtag>()
                .HasOne(pt => pt.Hashtag)
                .WithMany(t => t.AdvertHashtags)
                .HasForeignKey(pt => pt.HashtagId);
        }
        public DbSet<ApplicationProfile> ApplicationProfiles { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Advert> Adverts { get; set; }
        public DbSet<Hashtag> Hashtags { get; set; }
        public DbSet<IncomingMessage> IncomingMessages { get; set; }
        public DbSet<OutgoingMessage> OutgoingMessages { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<Image> Images { get; set; }
    }
}
