﻿using System;
using Ra_04.Domain.Interfaces;

namespace Ra_04.Infrastructure.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IProfileManager ProfileManager { get; }
        ICity CityRepository { get; }
        IRegion RegionRepository { get; }
        IDistrict DistrictRepository { get; }
        IAdvert AdvertRepository { get; }
        IAdvertService AdvertService { get; }
        IHashtag HashtagRepository { get; }
    }
}