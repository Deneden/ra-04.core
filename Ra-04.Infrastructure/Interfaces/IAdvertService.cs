﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ra_04.Domain.Models;
using Ra_04.Infrastructure.Models.AccountViewModels;
using Ra_04.Infrastructure.Models.AdvertModels;

namespace Ra_04.Infrastructure.Interfaces
{
    public interface IAdvertService
    {
        void Create(NewAdvertModel model, ApplicationUser user);
        IEnumerable<AdvertViewModel> GetAdvertViewModels(int count, int hashtag, string user);
        Task<AdvertDetailsModel> GetAdvertDetailsModels(int id, InfoViewModel user);
        Task AddView(int id);
        Task<string> GetAdvertUser(int id);
    }
}