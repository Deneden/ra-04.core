﻿using System;
using System.Collections.Generic;
using Ra_04.Domain.Models;

namespace Ra_04.Infrastructure.Models.AdvertModels
{
    public class AdvertViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Type { get; set; }
        public int Cost { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime ActualTime { get; set; }
        public int Views { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public ICollection<Hashtag> Hashtags { get; set; }
    }
}
