﻿using System;
using System.Collections.Generic;
using Ra_04.Domain.Models;

namespace Ra_04.Infrastructure.Models.AdvertModels
{
    public class AdvertDetailsModel
    {
        public int Id { get; set; }
        public string User { get; set; }
        public string UserEmail { get; set; }
        public int City { get; set; }
        public int Region { get; set; }
        public string Phone { get; set; }
        public int Type { get; set; }
        public int Cost { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime ActualTime { get; set; }
        public DateTime EdiTime { get; set; }
        public int Views { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public string BlockReason { get; set; }
        public ICollection<Hashtag> Hashtags { get; set; }
    }
}
