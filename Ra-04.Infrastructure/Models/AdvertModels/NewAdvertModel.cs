﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ra_04.Infrastructure.Models.AdvertModels
{
    public class NewAdvertModel
    {
        public int Type { get; set; }
        public int Cost { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public int Condition { get; set; }
        public string Hashtags { get; set; }
    }
}
