﻿using System;

namespace Ra_04.Infrastructure.Models.AccountViewModels
{
    public class InfoViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public string Website { get; set; }
        public string About { get; set; }
        public DateTime? RegisterTime { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public int RatingPlus { get; set; }
        public int RatingMinus { get; set; }
        public bool EmailConfirmed { get; set; }
        public byte[] LogoData { get; set; }
        public string LogoMimeType { get; set; }
        public int Region { get; set; }
        public int District { get; set; }
        public int City { get; set; }
        public string PostalCode { get; set; }
        public string Gender { get; set; }
    }
}
