﻿using System.ComponentModel.DataAnnotations;

namespace Ra_04.Infrastructure.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
