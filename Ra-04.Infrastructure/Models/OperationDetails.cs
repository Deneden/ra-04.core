﻿using Ra_04.Domain.Interfaces;

namespace Ra_04.Infrastructure.Models
{
    public class OperationDetails : IOperationDetails
{
    public OperationDetails(bool succedeed, string message, string prop)
    {
        Succedeed = succedeed;
        Message = message;
        Property = prop;
    }
    public bool Succedeed { get; }
    public string Message { get; }
    public string Property { get; }
}
}
