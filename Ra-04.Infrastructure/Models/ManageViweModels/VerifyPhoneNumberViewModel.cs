﻿using System.ComponentModel.DataAnnotations;

namespace Ra_04.Infrastructure.Models.ManageViweModels
{
    public class VerifyPhoneNumberViewModel
    {
        [Required]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Номер телефона")]
        public string PhoneNumber { get; set; }
    }
}
