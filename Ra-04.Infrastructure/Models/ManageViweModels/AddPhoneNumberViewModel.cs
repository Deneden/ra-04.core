﻿using System.ComponentModel.DataAnnotations;

namespace Ra_04.Infrastructure.Models.ManageViweModels
{
    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Номер телефона")]
        public string PhoneNumber { get; set; }
    }
}
