﻿using System.ComponentModel.DataAnnotations;

namespace Ra_04.Infrastructure.Models.ManageViweModels
{
    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Текущий пароль")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} должен быть минимум {2} и максимум {1} символов длиной.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтвердить новый пароль")]
        [Compare("NewPassword", ErrorMessage = "Новый пароль и подтверждение пароля не совпадают.")]
        public string ConfirmPassword { get; set; }
    }
}
