﻿namespace Ra_04.Infrastructure.Models.ManageViweModels
{
    public class RemoveLoginViewModel
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
    }
}
