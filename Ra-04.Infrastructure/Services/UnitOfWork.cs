﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Ra_04.Domain.Interfaces;
using Ra_04.Infrastructure.Context;
using Ra_04.Infrastructure.Interfaces;
using Ra_04.Infrastructure.ModelsReporitories;

namespace Ra_04.Infrastructure.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed;
        private readonly ApplicationContext _db;

        private readonly IProfileManager _profileManager;
        private readonly ICity _cityRepository;
        private readonly IDistrict _districtRepository;
        private readonly IRegion _regionRepository;
        private readonly IAdvert _advertRepository;
        private readonly IAdvertService _advertService;
        private readonly IHashtag _hashtagRepository;

        public UnitOfWork(ApplicationContext db, IHttpContextAccessor accessor)
        {
            _db = db;
            _cityRepository = new CityRepository(_db);
            _districtRepository = new DistrictRepository(_db);
            _regionRepository = new RegionRepository(_db);
            _advertRepository = new AdvertRepository(_db);
            _advertService = new AdvertService(this);
            _hashtagRepository = new HashtagRepository(_db);

        }

        public IProfileManager ProfileManager => _profileManager;
        public ICity CityRepository => _cityRepository;
        public IDistrict DistrictRepository => _districtRepository;
        public IRegion RegionRepository => _regionRepository;
        public IAdvert AdvertRepository => _advertRepository;
        public IAdvertService AdvertService => _advertService;
        public IHashtag HashtagRepository => _hashtagRepository;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                    _profileManager.Dispose();
                }
                _disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
