﻿using System.Threading.Tasks;

namespace Ra_04.Infrastructure.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
