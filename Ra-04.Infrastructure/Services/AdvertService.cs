﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Ra_04.Domain.Models;
using Ra_04.Infrastructure.Interfaces;
using Ra_04.Infrastructure.Models.AccountViewModels;
using Ra_04.Infrastructure.Models.AdvertModels;

namespace Ra_04.Infrastructure.Services
{
    public class AdvertService : IAdvertService
    {
        private readonly IUnitOfWork _uow;
        public AdvertService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public void Create(NewAdvertModel model, ApplicationUser user)
        {
            Advert advert = new Advert
            {
                ActualTime = DateTime.Now.AddDays(7),
                UserId = user.Id,
                Type = model.Type,
                Cost = model.Cost,
                CreateTime = DateTime.Now,
                EdiTime = DateTime.Now,
                Views = 0,
                Description = model.Description,
                Text = model.Text,
                IsBlock = false,
                BlockReason = "",
                IsDone = false,
                UserUseId = -1,
                IsDelete = false,
            };

            _uow.AdvertRepository.Create(advert);

            ICollection<Hashtag> hashtags = new List<Hashtag>();

            model.Hashtags = model.Hashtags.ToLower();
            model.Hashtags = model.Hashtags.Replace(",", " ");
            model.Hashtags = model.Hashtags.Replace("#", " ");

            string[] arrS =
                model.Hashtags.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);


            foreach (var str in arrS)
            {
                var hash = _uow.HashtagRepository.FindByName(str);
                if (hash == null) hash = _uow.HashtagRepository.Add(str);
            }
        }

        public IEnumerable<AdvertViewModel> GetAdvertViewModels(int count, int hashtag, string user)
        {
            IEnumerable<Advert> adverts;
            List<AdvertViewModel> advertView = new List<AdvertViewModel>();

            //    if (hashtag < 0)
            adverts = _uow.AdvertRepository.GetAdvertList(user);
            //     else adverts = _uow.AdvertRepository.GetAdvertByTag(hashtag);

            foreach (var ad in adverts)
            {
                if (hashtag < 0)
                {
                    advertView.Add(new AdvertViewModel
                    {
                        Id = ad.Id,
                        UserName = ad.User.UserName,
                        Type = "Продам", //AppConfiguration["TypeList:" + ad.Type.ToString()],
                        Cost = ad.Cost,
                        CreateTime = ad.CreateTime,
                        ActualTime = ad.ActualTime,
                        Views = ad.Views,
                        Description = ad.Description,
                        Text = ad.Text,
                        Hashtags = ad.AdvertHashtags.Select(x=>x.Hashtag).ToList()
                    });
                }
                else
                {
                    if (ad.AdvertHashtags.Select(x => x.Hashtag).Any(b => b.Id == hashtag))
                    {
                        advertView.Add(new AdvertViewModel
                        {
                            Id = ad.Id,
                            UserName = ad.User.UserName,
                            Type = "Продам", //AppConfiguration["TypeList:" + ad.Type.ToString()],
                            Cost = ad.Cost,
                            CreateTime = ad.CreateTime,
                            ActualTime = ad.ActualTime,
                            Views = ad.Views,
                            Description = ad.Description,
                            Text = ad.Text,
                            Hashtags = ad.AdvertHashtags.Select(x => x.Hashtag).ToList()
                        });
                    }
                }
            }
            if (advertView.Count > count) return advertView.GetRange(0, count);
            return advertView;
        }

        public async Task<AdvertDetailsModel> GetAdvertDetailsModels(int id, InfoViewModel user)
        {
            var advert = await _uow.AdvertRepository.GetAdvert(id);
            AdvertDetailsModel advertDetails = new AdvertDetailsModel
            {
                User = user.Name,
                UserEmail = user.Email,
                ActualTime = advert.ActualTime,
                BlockReason = advert.BlockReason,
                City = user.City,
                Cost = advert.Cost,
                CreateTime = advert.CreateTime,
                Description = advert.Description,
                EdiTime = advert.EdiTime,
                Hashtags = advert.AdvertHashtags.Select(x => x.Hashtag).ToList(),
                Id = advert.Id,
                Region = user.Region,
                Phone = user.PhoneNumber,
                Type = advert.Type,
                Views = advert.Views,
                Text = advert.Text
            };
            return advertDetails;
        }

        public async Task AddView(int id)
        {
            var advert = await _uow.AdvertRepository.GetAdvert(id);
            advert.Views += 1;
            _uow.AdvertRepository.Update(advert);
        }

        public async Task<string> GetAdvertUser(int id)
        {
            var advert = await _uow.AdvertRepository.GetAdvert(id);
            return advert.User.Email;
        }
    }
}
