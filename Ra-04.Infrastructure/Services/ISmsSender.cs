﻿using System.Threading.Tasks;

namespace Ra_04.Infrastructure.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}