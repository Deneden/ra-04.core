﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Ra_04.Infrastructure.Context;

namespace Ra04.Infrastructure.Migrations
{
    [DbContext(typeof(ApplicationContext))]
    partial class ApplicationContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.Advert", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("ActualTime");

                    b.Property<string>("BlockReason");

                    b.Property<int>("Cost");

                    b.Property<DateTime>("CreateTime");

                    b.Property<string>("Description");

                    b.Property<DateTime>("EdiTime");

                    b.Property<bool>("IsBlock");

                    b.Property<bool>("IsDelete");

                    b.Property<bool>("IsDone");

                    b.Property<string>("Text");

                    b.Property<int>("Type");

                    b.Property<int?>("UserId");

                    b.Property<string>("UserId1");

                    b.Property<int>("UserUseId");

                    b.Property<int>("Views");

                    b.HasKey("Id");

                    b.HasIndex("UserId1");

                    b.ToTable("Adverts");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.AdvertHashtag", b =>
                {
                    b.Property<int>("AdvertId");

                    b.Property<int>("HashtagId");

                    b.HasKey("AdvertId", "HashtagId");

                    b.HasIndex("HashtagId");

                    b.ToTable("AdvertHashtag");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.ApplicationProfile", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("About");

                    b.Property<string>("Address");

                    b.Property<DateTime?>("BirthDate");

                    b.Property<int>("City");

                    b.Property<int>("District");

                    b.Property<string>("Gender");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("LastLoginTime");

                    b.Property<byte[]>("LogoData");

                    b.Property<string>("LogoMimeType");

                    b.Property<string>("Patronymic");

                    b.Property<string>("PostalCode");

                    b.Property<int>("RatingMinus");

                    b.Property<int>("RatingPlus");

                    b.Property<int>("Region");

                    b.Property<DateTime?>("RegisterTime");

                    b.Property<string>("Surname");

                    b.Property<string>("Website");

                    b.HasKey("Id");

                    b.ToTable("ApplicationProfiles");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.City", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("DistrictId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("DistrictId");

                    b.ToTable("Cities");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.Comment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AdvertId");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("EditDate");

                    b.Property<bool>("IsDeleted");

                    b.Property<int>("RatingMinus");

                    b.Property<int>("RatingPlus");

                    b.Property<string>("Text");

                    b.Property<int?>("UserId");

                    b.Property<string>("UserId1");

                    b.HasKey("Id");

                    b.HasIndex("UserId1");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.District", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<int?>("RegionId");

                    b.HasKey("Id");

                    b.HasIndex("RegionId");

                    b.ToTable("Districts");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.Hashtag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Hashtags");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.Image", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AdvertId");

                    b.Property<string>("Path");

                    b.HasKey("Id");

                    b.HasIndex("AdvertId");

                    b.ToTable("Images");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.IncomingMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Text");

                    b.Property<int>("UserFromId");

                    b.Property<int?>("UserId");

                    b.Property<string>("UserId1");

                    b.HasKey("Id");

                    b.HasIndex("UserId1");

                    b.ToTable("IncomingMessages");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.Option", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AdvertId");

                    b.Property<int>("HashtagId");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.HasIndex("AdvertId");

                    b.ToTable("Options");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.OutgoingMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Text");

                    b.Property<int>("UserFromId");

                    b.Property<int?>("UserId");

                    b.Property<string>("UserId1");

                    b.HasKey("Id");

                    b.HasIndex("UserId1");

                    b.ToTable("OutgoingMessages");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.Region", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Regions");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Ra_04.Domain.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Ra_04.Domain.Models.Advert", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.ApplicationUser", "User")
                        .WithMany("Adverts")
                        .HasForeignKey("UserId1");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.AdvertHashtag", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.Advert", "Advert")
                        .WithMany("AdvertHashtags")
                        .HasForeignKey("AdvertId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Ra_04.Domain.Models.Hashtag", "Hashtag")
                        .WithMany("AdvertHashtags")
                        .HasForeignKey("HashtagId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Ra_04.Domain.Models.ApplicationProfile", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.ApplicationUser", "ApplicationUser")
                        .WithOne("ApplicationProfile")
                        .HasForeignKey("Ra_04.Domain.Models.ApplicationProfile", "Id")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Ra_04.Domain.Models.City", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.District", "District")
                        .WithMany("Cities")
                        .HasForeignKey("DistrictId");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.Comment", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.ApplicationUser", "User")
                        .WithMany("Comments")
                        .HasForeignKey("UserId1");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.District", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.Region", "Region")
                        .WithMany("Districts")
                        .HasForeignKey("RegionId");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.Image", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.Advert", "Advert")
                        .WithMany("Images")
                        .HasForeignKey("AdvertId");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.IncomingMessage", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.ApplicationUser", "User")
                        .WithMany("IncomingMessages")
                        .HasForeignKey("UserId1");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.Option", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.Advert", "Advert")
                        .WithMany("Options")
                        .HasForeignKey("AdvertId");
                });

            modelBuilder.Entity("Ra_04.Domain.Models.OutgoingMessage", b =>
                {
                    b.HasOne("Ra_04.Domain.Models.ApplicationUser", "User")
                        .WithMany("OutgoingMessages")
                        .HasForeignKey("UserId1");
                });
        }
    }
}
