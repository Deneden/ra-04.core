﻿using System.ComponentModel.DataAnnotations;

namespace Ra_04.Domain.Models
{
    public class Image
    {
        [Key]
        public int Id { get; set; }
        public int? AdvertId { get; set; }
        public Advert Advert { get; set; }
        public string Path { get; set; }
    }
}
