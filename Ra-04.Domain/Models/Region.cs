﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ra_04.Domain.Models
{
    public class Region
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<District> Districts { get; set; }
        public Region()
        {
            Districts = new HashSet<District>();
        }
    }
}
