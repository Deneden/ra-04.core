﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ra_04.Domain.Models
{
    public class Advert
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int Type { get; set; }
        public int Cost { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime ActualTime { get; set; }
        public DateTime EdiTime { get; set; }
        public int Views { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public bool IsBlock { get; set; }
        public string BlockReason { get; set; }
        public bool IsDone { get; set; }
        public int UserUseId { get; set; }
        public bool IsDelete { get; set; }
        public ICollection<AdvertHashtag> AdvertHashtags { get; set; }
        public ICollection<Option> Options { get; set; }
        public ICollection<Image> Images { get; set; }
        public Advert()
        {
            AdvertHashtags = new HashSet<AdvertHashtag>();
            Options = new HashSet<Option>();
            Images = new HashSet<Image>();
        }
    }
}
