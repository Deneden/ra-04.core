﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Ra_04.Domain.Models
{
    public class AdvertHashtag
    {
        [ForeignKey("Advert")]
        public int AdvertId { get; set; }
        public Advert Advert { get; set; }

        [ForeignKey("Hashtag")]
        public int HashtagId { get; set; }
        public Hashtag Hashtag { get; set; }
    }
}
