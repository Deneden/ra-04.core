﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Ra_04.Domain.Models
{
    public class ApplicationUser : IdentityUser
    {
        public virtual ApplicationProfile ApplicationProfile { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<IncomingMessage> IncomingMessages { get; set; }
        public ICollection<OutgoingMessage> OutgoingMessages { get; set; }
        public ICollection<Advert> Adverts { get; set; }

        public ApplicationUser()
        {
            Comments = new HashSet<Comment>();
            IncomingMessages = new HashSet<IncomingMessage>();
            OutgoingMessages = new HashSet<OutgoingMessage>();
            Adverts = new HashSet<Advert>();
        }
    }
}
