﻿using System.ComponentModel.DataAnnotations;

namespace Ra_04.Domain.Models
{
    public class City
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int? DistrictId { get; set; }
        public District District { get; set; }
    }
}
