﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ra_04.Domain.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public int? UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int AdvertId { get; set; }
        public string Text { get; set; }
        public bool IsDeleted { get; set; }
        public int RatingPlus { get; set; }
        public int RatingMinus { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime EditDate { get; set; }
    }
}
