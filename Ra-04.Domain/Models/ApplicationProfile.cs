﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ra_04.Domain.Models
{
    public class ApplicationProfile
    {
        [Key]
        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }
        public string About { get; set; }
        public DateTime? RegisterTime { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public int RatingPlus { get; set; }
        public int RatingMinus { get; set; }
        public bool IsDeleted { get; set; }
        public byte[] LogoData { get; set; }
        public string LogoMimeType { get; set; }
        public int Region { get; set; }
        public int District { get; set; }
        public int City { get; set; }
        public string PostalCode { get; set; }
        public string Gender { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
