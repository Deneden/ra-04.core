﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ra_04.Domain.Models
{
    public class OutgoingMessage
    {
        [Key]
        public int Id { get; set; }
        public int? UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int UserFromId { get; set; }
        public string Text { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
