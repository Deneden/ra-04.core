﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Ra_04.Domain.Models
{
    public class District
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int? RegionId { get; set; }
        public Region Region { get; set; }
        public ICollection<City> Cities { get; set; }
        public District()
        {
            Cities = new List<City>();
        }
    }
}
