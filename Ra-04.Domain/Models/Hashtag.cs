﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ra_04.Domain.Models
{
    public class Hashtag
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public  ICollection<AdvertHashtag> AdvertHashtags { get; set; }
        public Hashtag()
        {
            AdvertHashtags = new HashSet<AdvertHashtag>();
        }
    }
}
