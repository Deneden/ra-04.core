﻿using System.ComponentModel.DataAnnotations;

namespace Ra_04.Domain.Models
{
    public class Option
    {
        [Key]
        public int Id { get; set; }
        public int HashtagId { get; set; }
        public int? AdvertId { get; set; }
        public Advert Advert { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
