﻿using System;
using System.Threading.Tasks;
using Ra_04.Domain.Models;

namespace Ra_04.Domain.Interfaces
{
    public interface IProfileManager : IDisposable
    {
        Task<IOperationDetails> Create(ApplicationProfile userProfile);
        Task<IOperationDetails> Update(ApplicationProfile userProfile);
    }
}