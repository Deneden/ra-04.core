﻿namespace Ra_04.Domain.Interfaces
{
    public interface IOperationDetails
    {
        bool Succedeed { get; }
        string Message { get; }
        string Property { get; }
    }
}