﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ra_04.Domain.Models;

namespace Ra_04.Domain.Interfaces
{
    public interface IComment : IDisposable
    {
        IEnumerable<Comment> GetCommentList();
        Task<Region> GetComment(int id);
        void Create(Comment item);
        void Update(Comment item);
        void Delete(int id);
    }
}