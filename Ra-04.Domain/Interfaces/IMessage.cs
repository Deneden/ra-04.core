﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ra_04.Domain.Models;

namespace Ra_04.Domain.Interfaces
{
    public interface IMessage : IDisposable
    {
        IEnumerable<IncomingMessage> GetIncomingList();
        IEnumerable<OutgoingMessage> GetOutgoingList();
        Task<IncomingMessage> GetIncoming(int id);
        Task<OutgoingMessage> GetOutgoing(int id);
        void Create(OutgoingMessage item);
        void Update(OutgoingMessage item);
        void Delete(int id);
    }
}