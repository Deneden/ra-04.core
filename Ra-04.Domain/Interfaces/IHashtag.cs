﻿using System;
using Ra_04.Domain.Models;

namespace Ra_04.Domain.Interfaces
{
    public interface IHashtag : IDisposable
    {
        Hashtag FindByName(string name);
        Hashtag Add(string name);
        void Delete(int id);
    }
}