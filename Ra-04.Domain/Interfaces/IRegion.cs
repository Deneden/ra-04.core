﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ra_04.Domain.Models;

namespace Ra_04.Domain.Interfaces
{
    public interface IRegion : IDisposable
    {
        IEnumerable<Region> GetRegionList();
        Task<Region> GetRegion(int id);
        void Create(Region item);
        void Update(Region item);
        void Delete(int id);
    }
}