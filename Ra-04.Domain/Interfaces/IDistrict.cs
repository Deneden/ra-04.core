﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ra_04.Domain.Models;

namespace Ra_04.Domain.Interfaces
{
    public interface IDistrict : IDisposable
    {
        ICollection<District> GetDistrictList();
        Task<District> GetDistrict(int id);
        ICollection<District> GetDistrictByRegions(int id);
        void Create(District item);
        void Update(District item);
        void Delete(int id);
    }
}