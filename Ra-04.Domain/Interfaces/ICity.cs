﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ra_04.Domain.Models;

namespace Ra_04.Domain.Interfaces
{
    public interface ICity : IDisposable
    {
        ICollection<City> GetCitiesList();
        Task<City> GetCity(int id);
        ICollection<City> GetCitiesByDistrict(int id);
        void Create(City item);
        void Update(City item);
        void Delete(int id);
    }
}