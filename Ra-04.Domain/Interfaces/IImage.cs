﻿using Ra_04.Domain.Models;

namespace Ra_04.Domain.Interfaces
{
    public interface IImage
    {
        void Create(Image item);
        void Update(Image item);
        void Delete(int id);
    }
}