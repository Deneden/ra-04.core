﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ra_04.Domain.Models;

namespace Ra_04.Domain.Interfaces
{
    public interface IAdvert : IDisposable
    {
        IEnumerable<Advert> GetAdvertList(string id);
        IEnumerable<Advert> GetAdvertByTag(int hashtag);
        Task<Advert> GetAdvert(int id);
        void Create(Advert item);
        void Update(Advert item);
        void Delete(int id);
    }
}